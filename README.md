**Sùi mào gà là một căn bệnh nguy hiểm cho cả đàn ông lẫn phụ nữ, nó là nguyên nhân trực tiếp gây ung thư. Theo quan niệm người xa xưa, sùi mào gà được xem như là một căn bệnh mãi mãi đeo bám không thể nào trị khỏi. Nhưng với những tiến bộ của y học thời nay, bệnh sùi mào gà có thể chữa được bằng cả phương pháp đông y lẫn tây y.**

Và sau đây tôi xin chia sẽ một số bài thuốc đông y chữa sùi mào gà hiệu quả mà bản thân sưu tầm được.

1.Thuốc bôi trị [chữa sùi mào gà ở hậu môn](http://bacsidongygioi.com/trieu-chung-va-cach-chua-sui-mao-ga-o-hau-mon-206.html) bằng vỏ chuối

Cách thực hiện rất đơn giản, bạn chỉ cần chà xát vỏ chuối nhẹ nhàng vào khu vực bị ảnh hưởng hàng ngày. Nếu bạn thực hiện phương pháp này hãy kiên nhẫn vì sẽ mất vài tuần mới để có kết quả nhưng không triệt để.

2. Thuốc chữa sùi mào gà bằng giấm táo


Bạn nên sử dụng bông gòn có chứa dấm táo bôi vào khu vực bị ảnh hưởng và để qua đêm. Giấm táo có tính axit nên khi bôi có thể ảnh hưởng đến vùng da xung quanh, nếu bạn quá khó chịu thì không nên thực hiện phương pháp này liên tục vì nó cũng chỉ chữa tạm thời không dứt điểm.



3.Thuốc đặc trị sùi mào gà bằng khoai tây


Người bệnh chỉ cần cắt khoai tây thành những lát mỏng sau đó chà xát nhẹ nhàng vào vùng da bị ảnh hưởng, các mụn cóc sẽ biến mất sau khoảng vài ngày. Nhưng đây chỉ là biện pháp tạm thời.

Cúc vàng: Cúc vàng còn được gọi là dạ cúc vàng, vị cay đắng, hơi hàn, hơi độc. Về công dụng, sách " bản thảo hối ngôn" nói :" Tiêu nhọt giải độc, tẩy u bướu, có thể khư phong sát trùng", có thể thấy đây là những tính chất cơ bản giúp chữa sùi mào gà hiệu quả tạm thời.

![sùi_mào_gà](/uploads/2a749dc391061cd1bcf61aa28a4b0725/sùi_mào_gà.jpg)

4.Chữa sùi mào gà bằng tỏi

Cách chữa sùi mào gà bằng tỏi đơn giản nhất là thêm tỏi vào các món ăn hàng ngày như: các món xào, nước chấm, nước sốt...hoặc ăn tỏi sống.

Ngoài ra bạn cũng có thể trị sùi mào gà bằng nước cốt tỏi hoặc chế biến các món ăn từ tỏi vào khẩu phần ăn hàng ngày nhằm ngăn chạn sự phát triển của sùi mào gà tạm thời.

Hoặc dùng một lượng vừa phải tỏi tươi được đập nhỏ đem đắp vào những vị trị tổn thương sùi mào gà rồi giữ vững cố định băng gạc y tế. Lưu ý là nên dùng tỏi với một số lượng vừa phải, tránh đắp quá nhiều sẽ khiến vùng da quanh mụn sùi bị phỏng, rát và nên đắp kiên trì trong vòng 1 tuần.

Tỏi tuy có tác dụng chữa sùi mào gà rất tốt nhưng nếu chỉ dùng tỏi để chữa sùi mào gà thì không triệt để được.

Những cây thuốc có thể dùng làm thuốc bôi ngoài:

Địa phu tử: Địa phu tử còn gọi là địa quỳ, địa mạch, có công dụng thông lâm, trừ thấp nhiệt.

Sách" Bản thảo nguyên thủy" nói:" khư nhiệt tích trên da, trừ chứng ngứa trên da".

Sách " Thọ vực thần phương" còn ghi lại bài thuốc chữa trị tình trạng nổi mụn cóc:" Dùng địa phu tử, bạch phàn( phèn chua) sắc rửa nhiều lần."

Khổ sâm: Khổ sâm còn có tên địa hòe, vị đắng, tính hàn, không độc. Về công dụng, sách " Dược tính luận" nói:" Trị nhiệt độc phong, da dẻ phiền táo mà sinh ngứa", do đó nếu dùng làm thuốc bôi ngoài thì có thể đẩy lui các chứng bệnh về da như sùi mào gà 1 cách tạm thời.

Bạch hoa xà thiệt thảo: Bạch hoa xà thiệt thảo còn có tên bòi ngòi bò, có tác dụng kháng khuẩn, làm tăng sinh hệ tế bào nội bì lười, tăng hoạt lực của tế bào thực bào và tăng chức năng hệ miễn dịch. Ngoài ra, thuốc sắc bạch hoa xà thiệt thảo còn tác dụng tăng cường bạch cầu và tăng cường chức năng vỏ tuyến  thượng thận, nhờ đó mà kháng viêm và làm lặn các khối u, rất thích hợp để điều trị các u nhú của sùi mào gà giảm sùi mào gà tạm thời.

Nguồn: [http://bacsidongygioi.com/benh-sui-mao-ga-la-gi-dau-hieu-trieu-chung-nguyen-nhan-205.html](http://bacsidongygioi.com/benh-sui-mao-ga-la-gi-dau-hieu-trieu-chung-nguyen-nhan-205.html)